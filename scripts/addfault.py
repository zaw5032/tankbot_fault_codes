#!/usr/bin/env python



import roslib
import rospy
import sys
from tankbot.msg import IMU_10DOF
import re
import random
from std_msgs.msg import *
#Initialize an array for each type of bias

linbias=[0,0,0]
angbias=[0,0,0]
magbias=[0,0,0]
axisnum=0
typenum=0

#Initialize an array for noise added to each data value
noise=[0,0,0,0,0,0,0,0,0]

class add_fault:
   def __init__(self):


      #Set default settings and get overrides from parameter server if available
      self.fault_type=rospy.get_param('~fault_type', 'linear')
      self.axis=rospy.get_param('~axis', 'z')
      self.magnitude=rospy.get_param('~magnitude',0.0)

      self.gaussian_stddev=rospy.get_param('~gaussian_stddev', 0.0)
      self.gaussian_walk=rospy.get_param('~gaussian_walk', False)

      #Enumerate axis for array indexes (x:0 ,y:1, z:2)
      if (self.axis=='x'):axisnum=0
      elif (self.axis=='y'):axisnum=1
      elif(self.axis=='z'):axisnum=2

      #Enumerate fault types for array indexes (linear:0, angular:1, magnetic:2)
      #Add bias to the given type
      if (self.fault_type=='linear'):
	linbias[axisnum]=self.magnitude
	typenum=0
      elif(self.fault_type=='angular'):
	angbias[axisnum]=self.magnitude
	typenum=1
      elif(self.fault_type=='magnetic'):
	magbias[axisnum]=self.magnitude
	typenum=2
      
      #Use axis and fault type to determine which noise index to change
      self.msgindex=(3*typenum)+axisnum

      #Setup subscriber and publisher
      rospy.Subscriber("/TankbotArduino/IMU",IMU_10DOF, self.Fault_Callback)
      self.pub = rospy.Publisher('/Fault_IMU', IMU_10DOF)	

      self.pubinfo = rospy.Publisher('/Fault_Info', String)


   #Callback to handle IMU data 
   def Fault_Callback(self, msg):

	   #Reinitialize noise variable if not perforning a gaussian random walk. Otherwise add new noise to exixting noise
	   if not(self.gaussian_walk):noise[self.msgindex]=0


	   noise[self.msgindex]=noise[self.msgindex] + random.gauss(0, self.gaussian_stddev)
	   
	   #Create string for publishing fault information
	   info= "Type:%s | Axis:%s | Magnitude:%d | Noise: std dev=%d | Random Walk=%r" %(self.fault_type, self.axis, self.magnitude, self.gaussian_stddev, self.gaussian_walk)

	   self.pubinfo.publish(info)
	   #Add bias and noise to given value
	   msg.linear_acceleration.x=msg.linear_acceleration.x+linbias[0]+noise[0]
	   msg.linear_acceleration.y=msg.linear_acceleration.y+linbias[1]+noise[1]
	   msg.linear_acceleration.z=msg.linear_acceleration.z+linbias[2]+noise[2]

	   msg.angular_velocity.x=msg.angular_velocity.x+angbias[0]+noise[3]
	   msg.angular_velocity.y=msg.angular_velocity.y+angbias[1]+noise[4]
	   msg.angular_velocity.z=msg.angular_velocity.z+angbias[2]+noise[5]

	   msg.magnetic.x=msg.magnetic.x+magbias[0]+noise[6]
	   msg.magnetic.x=msg.magnetic.x+magbias[0]+noise[7]
	   msg.magnetic.x=msg.magnetic.x+magbias[0]+noise[8]

	   #Publish faulty data to /Fault_IMU
	   self.pub.publish(msg)
	

#Main function
def main(args):
  rospy.init_node('add_fault', anonymous=True)
  AddFault= add_fault()


  try:
    rospy.spin()
  except KeyboardInterrupt:
    print "Shutting down"


if __name__ == '__main__':
   main(sys.argv)
