#!/usr/bin/env python
import roslib
import sys
import rospy
import sys
from robot_messages.msg import *
from tankbot.msg import *
from std_msgs.msg import String
import serial
import time
from struct import *
import threading
import math
class info_log:

   def __init__(self):
	self.port_name=rospy.get_param("~port_name", "/dev/ttyUSB0")
	self.baud_rate=rospy.get_param("~baud_rate", 115200)
	self.ser=serial.Serial(self.port_name, self.baud_rate, timeout=1)

	#Initialize warning flags to default values
	#For 3 state failure effects: 1=healthy, 2=spikes, 3=long term error
	self.State1=1
	self.State2=1
	self.State3=1
	self.State4=1
	self.State5=1
	self.State6=1
	self.State7=1
	self.state_array=[1,1,3,3,2,0,1]

        #Initialize all flags to healthy in AGNC monitor
        for i in range(7):
          if i != 5:
	   self.ser.write(pack('cBB','E',i+1,self.state_array[i]))
	   time.sleep(0.2)


	#Initialize threshold variables for warning flags (Many more will have to be added and tuned)
	self.voltage_limit=5
	self.bot_width=0.5207
	self.last_encoder_count1=0
	self.last_encoder_count2=0
	self.encoder_constant_count=0
	self.encoder_change_count=0
	self.angular_from_encoders=0.01
	self.angular_from_IMU=0.0
	self.last_state=[0,0,0,0,0,0,0]
	self.motion_detect_threshold=.2
	self.time_since_consistency=0.0
	self.time_since_inconsistency=0.0
	self.window_size=60.0
	self.cur_crest_threshold=2.5
	self.cur_delta_threshold=10
	self.cur_spike=0
	self.alpha=(self.window_size-1)/self.window_size
	self.beta=1-self.alpha
	self.cur_MSval_right=0.0
	self.cur_MSval_left=0.0
	self.cur_spike=False
	self.cur_basically_zero=.2
	self.time_since_cur_spike=5001
	self.time_since_error_spike=501
	self.vel_error_spike=False
	self.vel_error_threshold=.01
	self.error_basically_zero=.05
	self.motion_detection=0.0
	self.MS_velocity_error=0.0
	self.vel_crest_threshold=2.5
	#Set up subscibers for data to determine current state of the Tankbot
	rospy.Subscriber("/TankbotArduino/IMU", IMU_10DOF, self.IMU_Callback)
	rospy.Subscriber("/MotorSpeeds", Float64MultiArrayStamped, self.Motor_Callback)
	rospy.Subscriber("/TankbotArduino/encoder",ENCODER, self.Encoder_Callback)
	rospy.Subscriber("/encoder_vel", Float64MultiArrayStamped, self.Vel_Callback)
        rospy.Timer(rospy.Duration(10),self.Timer_Callback, oneshot=False)
        rospy.Timer(rospy.Duration(.02),self.Timer_Callback2, oneshot=False)
	rospy.Subscriber("/CurrentArduino/MotorCurrent", Float64MultiArrayStamped, self.Current_Callback)
	
	#Set up publisher to output raw error information within ROS
	self.pub = rospy.Publisher('/Bayes_Info', String, queue_size=10)



	#rospy.Subscriber("/GPS_Heading",Float64Stamped, self.GPS_Callback)
  
   #Compares battery voltage against use set threshold to determine FE1
   def Motor_Callback(self, msg):
	batt_voltage=msg.array.data[6]
	if (batt_voltage<self.voltage_limit and batt_voltage > 0.0):
	  self.State1=2
	else:
	  self.State1=1

        if batt_voltage < 0.0:
	  self.State2=2
	else:
	  self.State2=1

   def Encoder_Callback(self, msg):

	#Maintain a count of consecutive samples in which a change occurred or a constant value was read
	#If within 5 of last measurement than reset change counter and increment constant counter
	#Otherwise reset constant counter and increment change counter. Also reset last value variables

	if (abs(msg.Encoder1-self.last_encoder_count1)<5)or(abs(msg.Encoder2-self.last_encoder_count2)<5):
	  self.encoder_constant_count+=1
	  self.encoder_change_count=0
	else:
	  self.encoder_constant_count=0
	  self.encoder_change_count+=1
	  self.last_encoder_count1=msg.Encoder1
	  self.last_encoder_count2=msg.Encoder2

	if self.State3==1:
	  if self.encoder_constant_count>0:
	    self.State3=2
	elif self.State3==2:
	  if self.encoder_constant_count>10:
	    self.State3=3
	  elif self.encoder_change_count>500:
	    self.State3=1
	else:
	  if self.encoder_change_count>1:
	    self.State3=2
	    self.encoder_change_count=0
	
	
   #Callback to handle current msgs. FE4 will come from this data
 
   def Current_Callback(self, msg):
	#right_current=(msg.array.data[0]*5.0/1024.0)*24.0-62.2
	#left_current=(msg.array.data[1]*5.0/1024.0)*24.0-62.2
	right_current=msg.array.data[0]*0.1171875-62.2
	left_current=msg.array.data[1]*0.1171875-62.2

	self.cur_MSval_right=(self.cur_MSval_right*self.alpha)+self.beta*pow(right_current,2)     
	self.cur_MSval_left=(self.cur_MSval_left*self.alpha)+self.beta*pow(left_current,2)
	cur_RMS_right=math.sqrt(self.cur_MSval_right)
	cur_RMS_left=math.sqrt(self.cur_MSval_left)
	cur_crest_factor_right=abs(right_current)/cur_RMS_right
	cur_crest_factor_left=abs(left_current)/cur_RMS_left

	if (cur_crest_factor_right>self.cur_crest_threshold):
		self.cur_spike=True
	elif (abs(right_current)-cur_RMS_right)>self.cur_delta_threshold:
		self.cur_spike=True
	elif (cur_crest_factor_left>self.cur_crest_threshold):
		self.cur_spike=True
	elif (abs(left_current)-cur_RMS_left)>self.cur_delta_threshold:
		self.cur_spike=True	
	else:
		self.cur_spike=False

	if (cur_RMS_right<self.cur_basically_zero or cur_RMS_left<self.cur_basically_zero):
		self.cur_spike=False


	if self.cur_spike:
	  self.time_since_cur_spike=0
	else:
	  self.time_since_cur_spike+=1

	if(cur_RMS_right<self.cur_basically_zero or cur_RMS_left<self.cur_basically_zero):
	  self.State4=3
	elif self.time_since_cur_spike<5000:
	  self.State4=2
	else:
	  self.State4=1




   #Callback to handle IMU msgs. Estimates angular velocity from IMU data for comparison with encoder estimate in timed callback
   #Currently uses user set noise threshold to determine rest or motion (FE5)
   def IMU_Callback(self, msg):

	self.angular_from_IMU=(msg.angular_velocity.z/360)*6.283
	
	#Check for motion from IMU and determine state of FE5
	self.motion_detection=self.motion_detection*.7+abs(.3*msg.linear_acceleration.x)
	if abs(self.motion_detection)<self.motion_detect_threshold:
		self.State5=2
	else:
		self.State5=1

   #Will most likely not be used at all
   '''
      def GPS_Callback(self, msg):

	this_variable_is_only_here_to_stop_indentation_errors=0
   '''

   #Determines FE3nfo_log by reading right and left encoder counts and frequency of errors

   #Mostly used for angular vlocity consistency checks against IMU data
   def Vel_Callback(self, msg):
	self.angular_from_encoders=(msg.array.data[1]-msg.array.data[0])/self.bot_width

   #Timed callback running at 50Hz. This will handle serial communications and anything that does not fall
   #into the categories of other callbacks	
   
   def Timer_Callback(self,msg):
		
	   rospy.loginfo(self.time_since_cur_spike)
	   #Send all fault effect states through serial port 
	    
	   self.state_array[0]=self.State1
	   self.state_array[1]=self.State2
	   self.state_array[2]=self.State3
	   self.state_array[3]=self.State4
	   self.state_array[4]=self.State5
	   self.state_array[5]=self.State6
	   self.state_array[6]=self.State7	  

	   for i in range(7):
	     if(self.state_array[i])!=self.last_state[i]:
	       if i != 5:
	            self.ser.write(pack('cBB','E',i+1,self.state_array[i]))
	            print(pack('cBB','E',i+1,self.state_array[i]))
	            time.sleep(0.2)
	     self.last_state[i]=self.state_array[i]

	   rospy.loginfo(self.state_array)
	   self.pub.publish(str(self.state_array))

   def Timer_Callback2(self,msg):	
	   velocity_error=self.angular_from_encoders-self.angular_from_IMU
	   self.MS_velocity_error=self.MS_velocity_error*self.alpha+self.beta*pow(velocity_error,2)
	   RMS_velocity_error=math.sqrt(self.MS_velocity_error)
	   vel_crest_factor=abs(velocity_error)/RMS_velocity_error
	   if vel_crest_factor>self.vel_crest_threshold:
	     self.vel_error_spike=True
	   else:
	     self.vel_error_spike=False
           if abs(velocity_error)<.1:
	     self.vel_error_spike=False
	   if self.vel_error_spike:
	      self.time_since_error_spike=0
	   else:
	      self.time_since_error_spike+=1

           if RMS_velocity_error > self.error_basically_zero:
		self.State7=3
	   elif self.time_since_error_spike<500:
		self.State7=2
	   else:
		self.State7=1
	   
def main(args):
  rospy.init_node('Bayesian_Info', anonymous=True)
  Info_Logger=info_log()
  S_Save = ''
  
  while not rospy.is_shutdown():
    num = Info_Logger.ser.inWaiting()
    if num > 0:
      String = Info_Logger.ser.read(num)
      print "Read from port: "
      print String

      for count in range(0,len(String)):

        S_Save = S_Save + String[count]
	
        if S_Save == "ROSCommCheck":
          time.sleep(0.2)
          Info_Logger.ser.write(S_Save)
          S_Save = ''
          #print "Printed:"
          #print S_Save

	elif S_Save[0] != "R" or len(S_Save)>12:
	  S_Save = ''

	  
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print "Shutting down"


if __name__ == '__main__':
  main(sys.argv)
