#!/usr/bin/env python

import roslib
import rospy
import sys
from tankbot.msg import IMU_10DOF
import re
import random
from std_msgs.msg import *

from dynamic_reconfigure.server import Server
from tankbot_faults.cfg import DynamFaultsConfig 

#The elements of these arrays are added to each data channel to control the level of bias and noise on each. At most one from each array will have a non-zero value during runtime

bias=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
noise=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
#Global variables used to enumuerate parameters and determine the index to assign values to
typenum=0
axisnum=0
stuck_param=.996
unstuck_param=.996
dropped_param=.8
det_stuck=0.0
det_dropped=0.0
last_msg=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]

class add_fault:

   def __init__(self):

   #Set default settings and get overrides from parameter server if available
      self.fault_type=rospy.get_param('~fault_type', 'linear')
      self.axis=rospy.get_param('~axis', 'x')
      self.magnitude=rospy.get_param('~magnitude',0.0)
      
      self.gaussian_stddev=rospy.get_param('~gaussian_stddev', 0.0)
      self.gaussian_walk=rospy.get_param('~gaussian_walk', False)
      self.drift_mag=rospy.get_param('~drift_mag', 0.0)
      self.stuck=rospy.get_param('~stuck', False)
      self.dropped=rospy.get_param('~dropped', False)
      self.imu_power=rospy.get_param('~imu_power', True)

      self.drop_bool=False
      self.stuck_bool=False

      #Enumerate fault types for array indexes (linear:0, angular:1, magnetic:2)
      if (self.fault_type=='linear'):
         typenum=0
      elif (self.fault_type=='angular'):
         typenum=1
      elif (self.fault_type=='magnetic'):
         typenum=2
      else: 
         typenum=0

      #Enumerate axis for array indexes (x:0 ,y:1, z:2)   
      if (self.axis=='x'):
         axisnum=0
      if (self.axis=='y'):
         axisnum=1
      if (self.axis=='z'):
         axisnum=2


      #Use axis and fault type to determine which noise index to change
      self.index=(3*typenum)+axisnum

      #Set up subscriber and publishers
      rospy.Subscriber("/TankbotArduino/IMU",IMU_10DOF, self.data_callback)

      self.pub = rospy.Publisher('/Fault_IMU', IMU_10DOF, queue_size=10)	
      self.pubinfo = rospy.Publisher('/Fault_Info', String, queue_size=10)

      #construct server to handle the configuration callback
      self.srv = Server(DynamFaultsConfig, self.config_callback)


   #Callback to handle IMU data 
   def data_callback(self, msg):

      #Create string for publishing fault information and publish the information to /Fault_Info	
      info= "Type:%s | Axis:%s | Magnitude:%f | Noise: std dev=%f | Random Walk=%d | Drift Magnitude=%f | Stuck Packets=%d | Dropped Packets=%d" %(self.fault_type, self.axis, self.magnitude, self.gaussian_stddev, self.gaussian_walk, self.drift_mag, self.stuck, self.dropped)
      self.pubinfo.publish(info)
      #Reinitialize noise variable if not performing a gaussian random walk. Otherwise add new noise to exixting noise
      if not(self.gaussian_walk):
         noise[self.index]=0

      #Add bias and noise to given value
      noise[self.index]=noise[self.index] + random.gauss((self.gaussian_walk)*self.drift_mag, self.gaussian_stddev)
      bias[self.index] = self.magnitude

      #Generate random numbers to determine sticks or drops      
      if(self.stuck):
	 det_stuck=random.random()
	 if (self.stuck_bool and det_stuck>stuck_param):
            self.stuck_bool=False
	 elif (not self.stuck_bool and det_stuck>unstuck_param):
            self.stuck_bool=True
      else:
         self.stuck_bool = False

      if(self.dropped):
	 det_dropped=random.random()
	 if(det_dropped>dropped_param):self.drop_bool=True
	 else:self.drop_bool=False
      else: 
         self.drop_bool = False

      if (not self.imu_power):self.drop_bool=True

      msg.linear_acceleration.x=(not self.drop_bool)*(msg.linear_acceleration.x+bias[0]+noise[0])
      msg.linear_acceleration.y=(not self.drop_bool)*(msg.linear_acceleration.y+bias[1]+noise[1])
      msg.linear_acceleration.z=(not self.drop_bool)*(msg.linear_acceleration.z+bias[2]+noise[2])

      msg.angular_velocity.x=(not self.drop_bool)*(msg.angular_velocity.x+bias[3]+noise[3])
      msg.angular_velocity.y=(not self.drop_bool)*(msg.angular_velocity.y+bias[4]+noise[4])
      msg.angular_velocity.z=(not self.drop_bool)*(msg.angular_velocity.z+bias[5]+noise[5])

      msg.magnetic.x=(not self.drop_bool)*(msg.magnetic.x+bias[6]+noise[6])
      msg.magnetic.x=(not self.drop_bool)*(msg.magnetic.x+bias[7]+noise[7])
      msg.magnetic.x=(not self.drop_bool)*(msg.magnetic.x+bias[8]+noise[8])

      rospy.loginfo(msg.linear_acceleration.z)
      if(self.stuck_bool):
        msg.linear_acceleration.x=last_msg[0]
        msg.linear_acceleration.y=last_msg[1]
        msg.linear_acceleration.z=last_msg[2]

	msg.angular_velocity.x=last_msg[3]
	msg.angular_velocity.y=last_msg[4]
	msg.angular_velocity.z=last_msg[5] 
      	
	msg.magnetic.x=last_msg[6]
      	msg.magnetic.x=last_msg[7]
      	msg.magnetic.x=last_msg[8]
      else:
	last_msg[0]=msg.linear_acceleration.x
	last_msg[1]=msg.linear_acceleration.y
	last_msg[2]=msg.linear_acceleration.z

	last_msg[3]=msg.angular_velocity.x
	last_msg[4]=msg.angular_velocity.y
	last_msg[5]=msg.angular_velocity.z

	last_msg[6]=msg.magnetic.x
	last_msg[7]=msg.magnetic.y
	last_msg[8]=msg.magnetic.z

     
      #Publish faulty data to /Fault_IMU
      rospy.loginfo(msg.linear_acceleration.z)
      self.pub.publish(msg)

   #Callback to handle parameter reconfiguration
   def config_callback(self,config, level):
    	typenum=config.fault_type
	axisnum=config.axis
	if(axisnum==0):
           self.axis='x'
	elif(axisnum==1):
           self.axis='y'
	elif(axisnum==2):
           self.axis='z'
        self.index=(3*typenum)+axisnum	

	if(typenum==0):
           self.fault_type="linear"
	elif(typenum==1):
           self.fault_type="angular"
	elif(typenum==2):
           self.fault_type="magnetic"   

	#Reinitialize array elements to 0 in case fault type or axis has changed
	for i in range(9):
	   bias[i]=0
	   noise[i]=0
	
	#Take values from configuration gui
	self.magnitude=config.bias
	self.gaussian_stddev=config.gaussian_stddev
	self.gaussian_walk=config.gaussian_walk
	self.imu_power=config.imu_power

        self.stuck = config.stuck
        self.dropped = config.dropped
        self.drift_mag = config.drift_mag

	return config

#Main function
def main(args):
  rospy.init_node('add_fault', anonymous=True)
  AddFault= add_fault()

  try:
    rospy.spin()
  except KeyboardInterrupt:
    print "Shutting down"

if __name__ == "__main__":
   main(sys.argv)



